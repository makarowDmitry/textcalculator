package ru.mda.textcalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    String firstNumber, secondNumber, result;
    EditText editTextOne;
    EditText editTextTwo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editTextOne = findViewById(R.id.edit_text1);
        editTextTwo = findViewById(R.id.edit_text2);
    }

    public void onClickButton(View view) {
        firstNumber = editTextOne.getText().toString();
        secondNumber = editTextTwo.getText().toString();

        result=Integer.toString(Integer.valueOf(firstNumber)+Integer.valueOf(secondNumber));

        Intent intent=new Intent(this, Calculator.class);
        intent.putExtra("firstNumber",firstNumber);
        intent.putExtra("secondNumber",secondNumber);
        intent.putExtra("result",result);
        startActivity(intent);
    }
}
